if (GetLocale() == "frFR") then
	cgc.LOCALS.DESCRIPTION = "Recherche dans la fen\195\170tre d�artisanat par nom d�objet ou par composant."

	-- Slash Command garbage.
	cgc.LOCALS.SLASH_FASTENCHANT = "Enchantement Rapide"
	cgc.LOCALS.SLASH_FASTENCHANT_DESC = "Active l'enchantement rapide en ne montrant plus la fen\195\170tre de confirmation."

	-- Frame Strings
	cgc.LOCALS.FRAME_SUBMIT_TEXT = "S"
	cgc.LOCALS.FRAME_RESET_TEXT = "R"
	cgc.LOCALS.FRAME_NO_RESULTS = "Aucune r\195\169alisation correspondant \195\160 votre crit\195\168re."
	cgc.LOCALS.FRAME_LINK_REAGENTS_TITLE = "Sur quel canal:"
	cgc.LOCALS.FRAME_LINK_REAGENTS = "Lien"
	cgc.LOCALS.FRAME_LINK_TYPES = {
		[1] = {"Guilde", "GUILD", nil},
		[2] = {"Groupe", "PARTY", nil}, 
		[3] = {"Dire", "SAY", nil, },
		[4] = {"Chuchoter", "WHISPER", "Indiquez le nom du joueur \195\160 qui vous souhaitez envoyer la liste des composants n\195\169cessaires \195\160 la r\195\169alisation."},
		[5] = {"Canal", "CHANNEL", "Indiquez le num\195\169ro du canal dans lequel vous souhaitez envoyer la liste des composants n\195\169cessaires \195\160 la r\195\169alisation. PS : _PAS_ le nom. Si le canal Commerce est /2, tapez simplement: 2"},
	}
	cgc.LOCALS.FRAME_SEARCH_TYPE_TITLE = "Recherchez par:"
	cgc.LOCALS.FRAME_SEARCH_TYPES = {
		["Name"] 		= { t="Nom", d="Recherche par le nom de l'objet dans la fen\195\170tre d'artisanat." },
		["Reagent"] 	= { t="Composant", d="Recherche par le nom des composants n\195\169cessaire \195\160 l'objet." },
		["Requires"] 	= { t="Outils", d="Recherche par le nom des outils requis pour cr\195\169er l'objet." },
		["Materials"] 	= { n="Materials", t="Have Materials |cffff0000*|r", d="Show only recipes that you currently have materials for.  Will reset automatically once the tradeskill/craft is closed or you may hit [R] to reset it as well.", 
			func=function() 
				cgc:Reset()
				cgc.setSearchType("Materials") -- Do not translate
				cgc.frame.SearchTypeButton:SetText("Materials") -- Ok to translate
				cgc:Search() 
			end, 
		hideCraft=true },
	}
end