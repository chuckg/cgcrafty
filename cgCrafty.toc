## Interface: 20003
## Title: cgCrafty |cff7fff7f -Ace2-|r
## Notes: Adding quick search functionality to the crafting window.
## Version: 2.0
## Author: chuckg (Blindchuck <Ropetown>)
## OptionalDeps: Ace2, DewdropLib
## X-Embeds: Ace2, DewdropLib
## X-eMail: c@chuckg.org
## X-Website: http://wowinterface.com/downloads/info4999-cgCrafty.html
## X-Category: Tradeskill
## X-Date: 2006-12-04
## X-AceForum: 2627
## X-RelSite-WoWI: 4999
## SavedVariables: cgCraftyDB
## LoadOnDemand: 1
## LoadWith: Blizzard_TradeSkillUI, Blizzard_CraftUI

libs\AceLibrary\AceLibrary.lua
libs\AceOO-2.0\AceOO-2.0.lua
libs\AceAddon-2.0\AceAddon-2.0.lua
libs\AceHook-2.1\AceHook-2.1.lua
libs\AceConsole-2.0\AceConsole-2.0.lua
libs\AceDB-2.0\AceDB-2.0.lua
libs\AceDebug-2.0\AceDebug-2.0.lua
libs\AceEvent-2.0\AceEvent-2.0.lua

libs\Dewdrop-2.0\Dewdrop-2.0.lua

cgCrafty.lua
cgCrafty-enUS.lua
cgCrafty-deDE.lua
cgCrafty-frFR.lua