-- Version
cgc.Version = "2.0.14"

-- Locals
cgc.LOCALS = {}
cgc.LOCALS.DESCRIPTION = "Search the craft window for items by name or reagent."

-- Slash Command garbage.
cgc.LOCALS.SLASH_FASTENCHANT = "Fast Enchant"
cgc.LOCALS.SLASH_FASTENCHANT_DESC = "Toggles fast enchanting by auto-accepting the replace enchant dialogue."

-- Frame Strings
cgc.LOCALS.FRAME_SUBMIT_TEXT = "S"
cgc.LOCALS.FRAME_RESET_TEXT = "R"
cgc.LOCALS.FRAME_NO_RESULTS = "No results matched your search."
cgc.LOCALS.FRAME_LINK_REAGENTS_TITLE = "To which channel:"
cgc.LOCALS.FRAME_LINK_REAGENTS = "Link"
cgc.LOCALS.FRAME_LINK_TYPES = {
	[1] = {"Guild", "GUILD", nil},
	[2] = {"Party", "PARTY", nil}, 
	[3] = {"Say", "SAY", nil, },
	[4] = {"Whisper", "WHISPER", "Type the name of the player you would like to send the reagent/material information to."},
	[5] = {"Channel", "CHANNEL", "Type in which channel number you would like to post the information to.  Note: _NOT_ the channel name. So if trade is /2, type simply: 2"},
}
cgc.LOCALS.FRAME_SEARCH_TYPE_TITLE = "Search By:"
cgc.LOCALS.FRAME_SEARCH_TYPES = {
	["Name"] 		= { t="Name", d="Search by the name of the item in the trade/craft window." },
	["Reagent"] 	= { t="Reagent", d="Search by the reagent that the item requires." },
	["Requires"] 	= { t="Requires", d="Search by the type of tools/items required to create the item." },
	["Materials"] 	= { n="Materials", t="Have Materials |cffff0000*|r", d="Show only recipes that you currently have materials for.  Will reset automatically once the tradeskill/craft is closed or you may hit [R] to reset it as well.", 
		func=function() 
			cgc:Reset()
			cgc.setSearchType("Materials")
			cgc.frame.SearchTypeButton:SetText("Materials")
			cgc:Search() 
		end, 
	hideCraft=true },
}


