if (GetLocale() == "deDE") then
	cgc.LOCALS.DESCRIPTION = "Das Handwerksfenster nach Gegenst\195\164nden (sortiert nach Name oder Reagenz) durchsuchen."

	-- Slash Command garbage.
	cgc.LOCALS.SLASH_FASTENCHANT = "Schnelle Verzauberung"
	cgc.LOCALS.SLASH_FASTENCHANT_DESC = "Schaltet schnelle Verzauberungen ein/aus, indem die Sicherheitsabfrage beim Verzaubern automatisch aktzeptiert wird."

	-- Frame Strings
	cgc.LOCALS.FRAME_SUBMIT_TEXT = "S"
	cgc.LOCALS.FRAME_RESET_TEXT = "R"
	cgc.LOCALS.FRAME_NO_RESULTS = "Keine \195\156bereinstimmungen f\195\188r deine Suche."
	cgc.LOCALS.FRAME_LINK_REAGENTS_TITLE = "In welchen Chatkanal:"
	cgc.LOCALS.FRAME_LINK_REAGENTS = "Link"
	cgc.LOCALS.FRAME_LINK_TYPES = {
		[1] = {"Gilde", "GUILD", nil},
		[2] = {"Gruppe", "PARTY", nil}, 
		[3] = {"Sagen", "SAY", nil, },
		[4] = {"Fl\195\188stern", "WHISPER", "Den Namen des Spielers eingeben, an den du die Informationen \195\188ber Reagenzien/Materialien senden willst."},
		[5] = {"Kanal", "CHANNEL", "Den Chatkanal angeben, in welchen du die Informationen senden willst.  Hinweis: NICHT den Namen des Channels. Wenn der Handelskanal zum Beispiel /2 ist, gib einfach ein: 2"},
	}
	cgc.LOCALS.FRAME_SEARCH_TYPE_TITLE = "Suche nach:"
	cgc.LOCALS.FRAME_SEARCH_TYPES = {
		["Name"] 		= { t="Name", d="Suche nach dem Namen des Items im Handels-/Handwerksfenster." },
		["Reagent"] 	= { t="Reagenz", d="Suche nach der Reagenz welches der Gegenstand ben\195\182tigt." },
		["Requires"] 	= { t="Ben\195\182tigt", d="Suche nach der Art der Materialien/Gegenst\195\164nde die ben\195\182tigt werden, um den Gegenstand herzustellen." },
		["Materials"] 	= { n="Materialien", t="Hat Materialien |cffff0000*|r", d="Zeigt nur Rezepte, f\195\188r die du aktuell die Materialien hast.  Setzt sich automatisch zur\195\188ck sobald das Tradeskill-/Craft-Fenster geschlossen wird, oder du die Taste [R] dr\195\188ckst.", 
			func=function() 
				cgc:Reset()
				cgc.setSearchType("Materials") -- Do not translate
				cgc.frame.SearchTypeButton:SetText("Materialien") -- Ok to translate
				cgc:Search() 
			end,
		hideCraft=true },
	}
end

